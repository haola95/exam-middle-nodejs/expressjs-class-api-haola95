// Import bo thu vien express
const express = require('express');

// Import data 
const { userClassList } = require('../../data.js')

// Import User Middleware
const { printUserURLMiddleware } = require("../routes/middlewares/userMiddleware.js");

const router = express.Router();

router.get("/users", printUserURLMiddleware, (request, response) => {
    let age = request.query.age;
    if (age) {
        // Khởi tạo biến result là kết quả tìm được 
        let result = [];

        result = userClassList.filter(data => {
            return (data.age > age)
        })
        response.status(200).json({
            users: result
        })
    } else {
        response.status(200).json({
            users: userClassList
        })
    }
});

router.post("/users", printUserURLMiddleware, (request, response) => {
    response.json({
        message: "POST new User"
    })
});

router.get("/users/:userId", printUserURLMiddleware, (request, response) => {
    let userId = request.params.userId;
    // Do userId nhận được từ params là string nên cần chuyển thành kiểu integer
    userId = parseInt(userId);
    if (!(Number.isInteger(userId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User Id is invalid"
        })
    } else {
        // Khởi tạo biến result là kết quả tìm được 
        let result = [];

        // Duyệt từng phần tử của mảng để tìm được User có Id tương ứng
        result = userClassList.filter(data => {
            return (data.checkUserById(userId))
        })
        response.status(200).json({
            status: "Success: Get User by Id: " + userId,
            data: result
        })
    }

});

router.put("/users/:userId", printUserURLMiddleware, (request, response) => {
    let userId = request.params.userId;

    response.json({
        message: "PUT user Id = " + userId
    })
});

router.delete("/users/:userId", printUserURLMiddleware, (request, response) => {
    let userId = request.params.userId;

    response.json({
        message: "DELETE user Id = " + userId
    })
});


// Export dữ liệu 1 module
module.exports = router;