// Khai báo class User
class User {
    constructor(paramId, paramName, paramPosition, paramOffice, paramAge, paramStartDate) {
        this.id = paramId;
        this.name = paramName;
        this.position = paramPosition;
        this.office = paramOffice;
        this.age = paramAge;
        this.startDate = paramStartDate;
    }
    checkUserById(paramId) {
        return this.id === paramId;
    }
}

let userClassList = [];

// Khởi tạo các class User
let classUser1 = new User(1, "Airi Satou", "Accountant", "Tokyo", 33 , "2008/11/28");
let classUser2 = new User(2, "Angelica Ramos", "Chief Executive Officer (CEO)", "London", 47 , "2009/10/09");
let classUser3 = new User(3, "Ashton Cox", "Junior Technical Author", "San Francisco", 66 , "2009/01/12");
let classUser4 = new User(4, "Bradley Greer", "Software Engineer", "London", 41 , "2012/10/13");
let classUser5 = new User(5, "Brenden Wagner", "Software Engineer", "San Francisco", 28 , "2011/06/07");
let classUser6 = new User(6, "Brielle Williamson", "Integration Specialist", "New York", 61 , "2012/12/02");
let classUser7 = new User(7, "Bruno Nash", "Software Engineer", "London", 38 , "2011/05/03");
let classUser8 = new User(8, "Caesar Vance", "Pre-Sales Support", "New York", 21 , "2011/12/12");
let classUser9 = new User(9, "Cara Stevens", "Sales Assistant", "New York", 46 , "2011/12/06");
let classUser10 = new User(10, "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", 22 , "2012/03/29");


userClassList.push(classUser1, classUser2, classUser3, classUser4, classUser5, classUser6, classUser7, classUser8, classUser9, classUser10);

module.exports = { userClassList };
    

